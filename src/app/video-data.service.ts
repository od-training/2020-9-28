import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Video } from './dashboard/models';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  url = 'https://api.angularbootcamp.com/videos';

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(this.url);
  }
}
