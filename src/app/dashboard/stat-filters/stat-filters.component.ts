import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { debounceTime, tap } from 'rxjs/operators'
import { FormBuilder, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent implements OnInit {

  nameFilterControl: FormControl = this.fb.control('');

  controlValue$: Observable<string>;

  @Output() filterChange = new EventEmitter<string>();

  constructor(private fb: FormBuilder) {
    this.controlValue$ = this.nameFilterControl.valueChanges.pipe(
      tap(nameValue => console.log('before', {nameValue})),
      debounceTime(300),
      tap(nameValue => {
        this.filterChange.emit(nameValue);
      })
    );
  }

  ngOnInit(): void {
  }
}
