import { Component, OnInit } from '@angular/core';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { VideoDataService } from '../../video-data.service';

import { Video } from '../models';
@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent implements OnInit {
  // Needs selectedVideo state
  selectedVideo: Video;
  dashboardVideos: Observable<Video[]> = this.videoDataService.loadVideos();
  filter = new Subject<string>();

  filteredVideos = combineLatest([this.filter, this.dashboardVideos]).pipe(
    map( ([filterTerm, videos]) => videos.filter(video => {
      const searchTerm = filterTerm?.toLocaleLowerCase() || '';
      const title = video?.title?.toLocaleLowerCase() || '';
      return title.includes(searchTerm);
    }),
    tap(videos => console.log({videos})),
    )
  );

  constructor(private videoDataService: VideoDataService ) { }

  ngOnInit(): void {
  }

  handleSelectVideo(video: Video): void {
    this.selectedVideo = video;
  }

  handleFilter(name: string): void {
    this.filter.next(name);
  }
}
