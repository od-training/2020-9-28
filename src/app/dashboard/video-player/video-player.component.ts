import { Component, Input, OnInit } from '@angular/core';
import { Video } from '../models';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {

  @Input() playerSelectedVideo: Video;
  constructor() { }

  ngOnInit(): void {  }

}
