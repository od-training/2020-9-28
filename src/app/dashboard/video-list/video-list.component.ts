import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Video } from '../models';


@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {

  @Input() videos: Video[];
  @Input() selectedVideo: Video;
  // needs to emit event when video is selected
  @Output() selectVideo = new EventEmitter<Video>();

  constructor() { }

  ngOnInit(): void {
  }

  emitSelectedVideo(video: Video): void {
    this.selectVideo.emit(video);
  }

  findVideo(name: string): Video {
    return this.videos.find((video) => {
      return video.title === name;
    });
  }

  getTitles(): string[] {
    return this.videos.map(video => video.title);
  }

}
